<?php

use Service\DotEnv;
use Renderer\Render;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$stripe = new StripeClient($dotEnv['STRIPE_SK']);

if (isset($_GET['id']) && isset($_GET['data'])) {
  if($_GET['data']=== 'subscription'){
    $resp = $stripe->subscriptions->retrieve($_GET['id']);
  }else if($_GET['data']=== 'product'){
    $resp = $stripe->products->retrieve($_GET['id']);
  }else if($_GET['data']=== 'price'){
    $resp = $stripe->prices->retrieve($_GET['id']);
  }
  $json_pretty = json_encode($resp, JSON_PRETTY_PRINT);
}

?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Stripe TEST</h1>
    <hr>
    <h2 class="mt-5"><?= $_GET['data'] ?> details</h2>
    <a href="index.php">Back</a><br><br>
    <pre><?= $json_pretty ?><pre/>
