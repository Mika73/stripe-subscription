<?php

use Renderer\Render;
use Service\DotEnv;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$stripe = new StripeClient($dotEnv['STRIPE_SK']);

$products = $stripe->products->all();

if (isset($_POST['submit'])) {
  $stripe->prices->create([
    'unit_amount' => floatval(htmlspecialchars(trim($_POST["pprice"]))) * 100,
    'currency' => htmlspecialchars(trim($_POST["currency_code"])),
    'recurring' => [
      'interval' => htmlspecialchars(trim($_POST["interval_unit"]))
    ],
    'product' => htmlspecialchars(trim($_POST["pid"])),
  ]);
  header('Location: index.php');
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Create Price </h1>

    <form method="post">

      <div class="form-group">
        <label class="h5" for="pid">Product:</label>
        <select class="form-select" name="pid" id="pid">
          <?php
          foreach ($products->data as $product) {
            echo " <option value='$product->id'>$product->name</option>";
          }
          ?>
        </select>
      </div>

      <div class="form-group mt-5">
        <label for="pprice">Product price:</label>
        <input class="form-control" type="number" step=0.01 id="pprice" name="pprice">
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="currency_code">Currency</label>
        <input class="form-control" type="text" id="currency_code" name="currency_code">
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="interval_unit">Interval at which the subscription is charged or billed</label>
        <select class="form-select" name="interval_unit" id="interval_unit">
          <option value="day">DAY ( A daily billing cycle )</option>
          <option value="week">WEEK ( A weekly billing cycle )</option>
          <option value="month">MONTH ( A monthly billing cycle )</option>
          <option value="year">YEAR ( A yearly billing cycle )</option>
        </select>
      </div>

      <input class="btn btn-secondary mt-5" type="submit" name="submit" value="Create">
    </form>
    <div class="my-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
