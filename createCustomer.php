<?php

use Renderer\Render;
use Service\DotEnv;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

if (isset($_POST['submit'])) {

  $dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
  $stripe = new StripeClient($dotEnv['STRIPE_SK']);

  $stripe->customers->create([
    'name' => htmlspecialchars(trim($_POST["cname"])),
    'email' => htmlspecialchars(trim($_POST["cmail"]))
  ]);
  header('Location: index.php');
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Create Customer </h1>

    <form method="post">

      <div class="form-group  mt-5">
        <label for="cname">Customer name:</label>
        <input class="form-control" type="text" id="cname" name="cname">
      </div>

      <div class="form-group  mt-5">
        <label for="cmail">Customer email:</label>
        <input class="form-control" type="text" id="cmail" name="cmail">
      </div>

      <input class="btn btn-secondary mt-5" type="submit" name="submit" value="Create">
    </form>
    <div class="my-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
