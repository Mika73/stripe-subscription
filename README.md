# Stripe subscription
This app is for building Stripe continuous payments.
[Stripe subscription](https://stripe.com/docs/billing/subscriptions/overview)
## Requirement
- PHP >= 7.4
- MySQL 8.0
- Stripe account
## How to set up the application
1. Create database
Execute the following command under the root directory
```
mysql -u username -p < stripe.sql
```
2. Create .env file
```
DATABASE_DNS=mysql:host=localhost;dbname=stripe;charset=utf8mb4
DATABASE_USER= //Your database user name
DATABASE_PASSWORD= //Your database password
STRIPE_SK= //Stripe secret key
WEBHOOK_SECRET= //Leave blank if entering from application
```
3. Install dependencies
```
composer install
```
4. Create webhook
- From the top page, click the create webhook button to register the webhook required for the subscription.
## Local test
[ngrok](https://ngrok.com/) is useful for testing webhooks in a local environment
```
ngrok http http://localhost:3000
```
