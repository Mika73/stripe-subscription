<?php

use Service\Db;
use Service\DotEnv;
use Renderer\Render;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$stripe = new StripeClient($dotEnv['STRIPE_SK']);
$db = new Db();

// Deactivate a subscription
if (isset($_GET['subscription_action']) && isset($_GET['id'])) {
  if ($_GET['subscription_action'] === 'deactivate') {
    $resp = $stripe->subscriptions->cancel($_GET['id']);
    $db->updateSubscription($_GET['id'], $resp->status, json_encode($resp));
  }
  header('Location: /');
}

// Create webhook
if (isset($_GET['create_webhook']) && $_GET['create_webhook'] === 'true') {

  $resp = $stripe->webhookEndpoints->all();

  foreach ($resp->data as $webhook) {
    $stripe->webhookEndpoints->delete(
      $webhook->id,
      []
    );
  }

  // Delete webhookid of .env
  $dotEnvFile = file('./.env');
  $i = 0;
  $lineNo = 0;
  foreach ($dotEnvFile as $line) {
    if (substr($line, 0, 14) === 'WEBHOOK_SECRET') {
      $lineNo = $i;
      break;
    }
    $i++;
  }
  unset($dotEnvFile[$lineNo]);
  file_put_contents('./.env', $dotEnvFile);

  $location = $_SERVER['HTTP_HOST'];
  if (substr($location, 0, 9) === 'localhost') {
    $webhookUrl = "http://$location/createSubscription.php?webhook=true";
  } else {
    $webhookUrl = "https://$location/createSubscription.php?webhook=true";
  }

  $resp = $stripe->webhookEndpoints->create([
    'url' => $webhookUrl,
    'enabled_events' => [
      'checkout.session.completed',
      'invoice.paid',
      'invoice.payment_failed',
      'customer.subscription.deleted'
    ],
  ]);
  $webhookSecret = $resp->secret;

  // Register webhook ID in .env
  file_put_contents("./.env", "WEBHOOK_SECRET=$webhookSecret", FILE_APPEND);
}

// Fetch all products and customers
$products = $stripe->products->all();
$prices = $stripe->prices->all();
$subscriptions = $stripe->subscriptions->all();
$subscriptionList = $db->findAllSubscription();

?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Stripe TEST</h1>
    <hr>
    <h2 class="mt-5">Product</h2>
    <table class="table table-striped">
      <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Show</th>
      </tr>
      <?php
      foreach ($products->data as $pr) {
        $metadata = $pr->metadata;
        $amount = intval($metadata->amount) / 100;
        echo "
        <tr>
          <td>$pr->id</td>
          <td>$pr->name</td>
          <td>$amount $metadata->currency</td>
          <td><a href='show.php?id=$pr->id&data=product'>Show</a></td>
        </tr>";
      }
      ?>
    </table>

    <a href="createProduct.php">Create a new product</a>

    <hr>

    <h2 class="mt-5">Price</h2>
    <table class="table table-striped">
      <tr>
        <th>Price ID</th>
        <th>Product ID</th>
        <th>Price</th>
        <th>Recurring</th>
        <th>Show</th>
      </tr>
      <?php
      foreach ($prices->data as $pri) {
        $amount = intval($pri->unit_amount) / 100;
        $recurring = $pri->recurring;
        echo "
        <tr>
          <td>$pri->id</td>
          <td>$pri->product</td>
          <td>$amount $pri->currency</td>
          <td>$recurring->interval_count per $recurring->interval</td>
          <td><a href='show.php?id=$pri->id&data=price'>Show</a></td>
        </tr>";
      }
      ?>
    </table>

    <a href="createPrice.php">Create a new price</a>

    <hr>

    <h2 class="mt-5">Subscriptions(Local DB)</h2>

    <table class="table table-striped">
      <tr>
        <th>Subscription ID</th>
        <th>Stripe session ID</th>
        <th>Status</th>
        <th>Show</th>
        <th>Action</th>
      </tr>
      <?php
      foreach ($subscriptionList as $sub) {
        $subData = json_decode($sub['resource']);
        $subSessionId = $sub['stripe_session_id'];
        $subStatus = $sub['status'];
        $payment = json_decode($sub['resource']);

        echo "
        <tr>
          <td>$subData->id</td>
          <td>$subSessionId</td>
          <td>$subStatus</td>
          <td><a href='show.php?id=$subData->id&data=subscription'>Show</a></td>";
        if ($subData->canceled_at) {
          echo  "<td></td></tr>";
        }else{
          echo  "<td><a href='/?subscription_action=deactivate&id=$subData->id'>Cancel</a></td></tr>";
        }
      }
      ?>
    </table>

    <div class="mb-5">
      <a href='/?create_webhook=true'>Create webhook</a><br>
      <a href="createSubscription.php">New subscription</a>
    </div>

    <hr>

    <h2 class="mt-5">Subscriptions(Stripe DB)</h2>

    <table class="table table-striped mb-5">
      <tr>
        <th>Subscription ID</th>
        <th>Price</th>
        <th>Status</th>
        <th>Show</th>
      </tr>
      <?php
      foreach ($subscriptions->data as $subs) {
        $amount = $subs->items->data[0]->plan->amount / 100;
        $currency = $subs->items->data[0]->plan->currency;
        $status = $subs->items->data[0]->plan->active ? 'active' : 'canceled';
        echo "
    <tr>
      <td>$subs->id</td>
      <td>$amount $currency</td>
      <td>$subs->status</td>
      <td><a href='show.php?id=$subs->id&data=subscription'>Show</a></td>
    </tr>";
      }
      ?>
    </table>
</body>

</html>
