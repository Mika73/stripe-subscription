<?php

use Renderer\Render;
use Service\DotEnv;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

if (isset($_POST['submit'])) {

  $dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
  $stripe = new StripeClient($dotEnv['STRIPE_SK']);

  $stripe->products->create([
    'name' => htmlspecialchars(trim($_POST["pname"])),
    "metadata" => [
      "amount" => floatval(htmlspecialchars(trim($_POST["pprice"]))) * 100,
      "currency" => "EUR"
    ]
  ]);
  header('Location: index.php');
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Create Product </h1>

    <form method="post">

      <div class="form-group  mt-5">
        <label for="pname">Product name:</label>
        <input class="form-control" type="text" id="pname" name="pname">
      </div>

      <div class="form-group mt-5">
        <label for="pprice">Product price:</label>
        <input class="form-control" type="number" step=0.01 id="pprice" name="pprice">
      </div>

      <input class="btn btn-secondary mt-5" type="submit" name="submit" value="Create">
    </form>
    <div class="my-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
