CREATE DATABASE IF NOT EXISTS stripe;
USE stripe;
CREATE TABLE IF NOT EXISTS `Subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_session_id` varchar(255) NOT NULL,
  `stripe_subscription_id` varchar(255),
  `status` varchar(255) NOT NULL,
  `resource` JSON,
  PRIMARY KEY (`id`)
)
