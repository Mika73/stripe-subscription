<?php

namespace Service;

use PDO;
use PDOException;
use Service\DotEnv;

class Db
{
  public static $pdo = null;

  public function __construct()
  {
    if (self::$pdo === null) {
      $dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();

      $database = $dotEnv['DATABASE_DNS'];
      $databaseUser = $dotEnv['DATABASE_USER'];
      $databasePassword = $dotEnv['DATABASE_PASSWORD'];

      try {
        $db = new PDO($database, $databaseUser, $databasePassword);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      } catch (PDOException $e) {
        echo "Connection failed : " . $e->getMessage();
      }
      self::$pdo = $db;
    }
  }

  public function createSubscription($sessionId, $status)
  {
    try {
      $query = 'INSERT INTO Subscription SET stripe_session_id = :stripe_session_id, status = :status';
      $stmt = self::$pdo->prepare($query);
      $stmt->bindParam(':stripe_session_id', $sessionId, PDO::PARAM_STR);
      $stmt->bindParam(':status', $status, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
  }

  public function updateSubscriptionWithSessionId($sessionId, $subscriptionId, $status, $payment)
  {
    try {
      $query = 'UPDATE Subscription SET status = :status,  stripe_subscription_id = :stripe_subscription_id, resource = :resource WHERE stripe_session_id = :stripe_session_id';
      $stmt = self::$pdo->prepare($query);
      $stmt->bindParam(':status', $status, PDO::PARAM_STR);
      $stmt->bindParam(':resource', $payment, PDO::PARAM_STR);
      $stmt->bindParam(':stripe_session_id', $sessionId, PDO::PARAM_STR);
      $stmt->bindParam(':stripe_subscription_id', $subscriptionId, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
  }

  public function updateSubscription($subscriptionId, $status, $payment)
  {
    try {
      $query = 'UPDATE Subscription SET status = :status, resource = :resource WHERE stripe_subscription_id = :stripe_subscription_id';
      $stmt = self::$pdo->prepare($query);
      $stmt->bindParam(':status', $status, PDO::PARAM_STR);
      $stmt->bindParam(':resource', $payment, PDO::PARAM_STR);
      $stmt->bindParam(':stripe_subscription_id', $subscriptionId, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
  }

  public function findAllSubscription()
  {
    try {
      $query = 'SELECT * FROM Subscription ORDER BY id DESC';
      $stmt = self::$pdo->prepare($query);
      $stmt->execute();
      $list = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
    return $list;
  }
}
