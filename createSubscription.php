<?php

use Service\Db;
use Service\DotEnv;
use Stripe\Webhook;
use Renderer\Render;
use Stripe\StripeClient;

require_once 'vendor/autoload.php';
require_once 'Class/autoload.php';

$db = new Db();

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$stripe = new StripeClient($dotEnv['STRIPE_SK']);

$prices = $stripe->prices->all();

if (isset($_GET['id'])) {

  $location = $_SERVER['HTTP_HOST'];

  if (substr($location, 0, 9) === 'localhost') {
    $url = "http://$location/createSubscription.php";
  } else {
    $url = "https://$location/createSubscription.php";
  }

  $session = $stripe->checkout->sessions->create([
    'success_url' => $url . '?success=true',
    'cancel_url' => $url . '?success=false',
    'line_items' => [
      [
        'price' => htmlspecialchars(trim($_GET['id'])),
        'quantity' => 1,
      ],
    ],
    'mode' => 'subscription',
  ]);

  // Stores order details in database
  $status = 'checkout.session.created';
  $db->createSubscription($session->id, $status);

  header("Location: " . $session->url);
}

// Stripe subscription completed. Display success or error message
if (isset($_GET['success']) && $_GET['success'] === 'true') {
  $_SESSION['flash_message'] = "Payment was successfully completed";
}
if (isset($_GET['success']) && $_GET['success'] === 'false') {
  $_SESSION['flash_err_message'] = "Payment failed";
}

// Handle Stripe webhook
if (isset($_GET['webhook']) && $_GET['webhook'] === 'true') {

  $payload = file_get_contents('php://input');
  $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
  $event = null;
  $endpoint_secret = $dotEnv['WEBHOOK_SECRET'];

  try {
    $event = Webhook::constructEvent(
      $payload,
      $sig_header,
      $endpoint_secret
    );
  } catch (\UnexpectedValueException $e) {
    // Invalid payload
    http_response_code(400);
    exit('{Invalid payload}');
  } catch (\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
    http_response_code(400);
    exit('{Invalid signature}');
  }

  // Update order details in database

  $status = $event->type;

  $resp = $stripe->subscriptions->retrieve($event->data->object->subscription);

  if ($event->type === 'checkout.session.completed') {
    $db->updateSubscriptionWithSessionId($event->data->object->id, $event->data->object->subscription, $status, json_encode($resp));
  } else if ($event->type === 'customer.subscription.deleted') {
    $db->updateSubscription($event->data->object->id, $status, json_encode($resp));
  } else {
    $db->updateSubscription($event->data->object->subscription, $status, json_encode($resp));
  }
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Stripe TEST</h1>
    <hr>
    <h2>Subscription</h2>
    <?= Render::flashMessage() ?>
    <table class="table table-striped">
      <tr>
        <th>Price ID</th>
        <th>Price</th>
        <th>Recurring</th>
        <th>Action</th>
      </tr>
      <?php
      foreach ($prices->data as $pri) {
        $price = $pri->unit_amount / 100;
        $recurring = $pri->recurring;

        echo "
        <tr>
          <td>$pri->id</td>
          <td>$price $pri->currency</td>
          <td>$recurring->interval_count per $recurring->interval</td>";
        if ($pri->active === true) {
          echo "<td><a href='createSubscription.php?id=$pri->id'>Subscribe</a></td></tr>";
        } else {
          echo "<td></td></tr>";
        }
      }
      ?>
    </table>
    <div class="mb-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
